var Carrito = function(widget, itemspanel, prodsinit){
	var self = this;
	var counter = $(widget).find(".carritonitems").first();
	var products = [];
	var cartlistTMPL = null;

	var updateToStored = function(action, dataTostore = null, listener = null){
		$.ajax({
	       url:"./scripts/php/carrito.php",
	       type:"POST",
	       dataType:"json",
	       data:{"action":action,"data":dataTostore},
	       cache:false,
	       error:(error,req)=>{},
	       timeout:(resp)=>{},
	       success:(cartdata, req)=>{
	        try{
	          if(typeof listener == "function")	listener(cartdata);
	        } catch(ex){
	          alert("Error al modificar el carrito, por favor vuelva más tarde");
	        }
	        
	       }
	    });
	}
	var updateCounter = function(){
		counter.html(products.length);
	}
	var findByid= function(pid){
		for(var prod of products){
			if(prod["id"]===pid)	return prod;
		}
		return false;
	}
	this.init= function(){
		for(var pini of prodsinit){
			self.load(pini);
		}
		updateCounter();
		$.get(window.viewsURL+"cartlist.html",(clHTML)=>{
			cartlistTMPL= $.templates(clHTML);
		});
	}

	this.listProducts= function(){
		if(cartlistTMPL){
			var total = 0.0;
			for(var prod of products){
				let price = ($.isNumeric(prod["price"])? prod["price"]:parseFloat(prod["price"].trim().substring(1).replace(",","")));
				total +=  price * parseInt(prod["cantidad"]);
			}
			var clrendered= cartlistTMPL.render({"productos":products, "total":total});
			itemspanel.find(".modal-body").empty().html(clrendered);
			itemspanel.find(".modal-body").find(".sclbtn").click(function(){
				switch($(this).data("action")){
					case "del":
						self.eliminar($(this).data("target"));
					break;
					case "dec":
						self.decrementar($(this).data("target"));
					break;
					case "inc":
						self.incrementar($(this).data("target"));
					break;
				}
			});
			itemspanel.modal("show");
			if(products.length==0)	itemspanel.find(".paybtn").first().prop("disabled",true);
			else 	itemspanel.find(".paybtn").first().prop("disabled",false);
		}
	}
	this.load= function(pdata){
		products.push(pdata);
	}
	
	this.add = function(pdata){
		var inprods = findByid(pdata["id"]);
		if(!inprods){
			pdata["cantidad"]=1.0;
			products.push(pdata);
		} else{
			inprods["cantidad"] = parseInt(inprods["cantidad"])+1.0;
		}
		updateToStored("set",products,(response)=>{
			updateCounter();
		});
	}
	this.incrementar = function(pid){
		var inprods = findByid(pid);
		if(inprods){
			inprods["cantidad"] = parseInt(inprods["cantidad"])+1.0;
		}
		updateToStored("set", products,()=>{
			self.listProducts();
		});
	}
	this.decrementar = function(pid){
		var inprods = findByid(pid);
		if(inprods){
			inprods["cantidad"] = parseInt(inprods["cantidad"])-1.0;
		}
		updateToStored("set", products,()=>{
			self.listProducts();
		});
	}
	this.eliminar = function(pid){
		var inprods = findByid(pid);
		if(inprods){
			var prodindex = products.indexOf(inprods);
			if(prodindex!=-1){
				products.splice(prodindex,1);
			}
		}
		updateToStored("set", products,()=>{
			self.listProducts();
		});
	}
	this.reset = function(){
		products.splice(0);
		updateCounter();
		self.listProducts();
		itemspanel.modal("hide");
	}
	self.init();
	return self;
}