(function ( $ ) {
    $.fn.cargando = function() {
        var self = this;
        var sp_html = '<div class="sp-cont"><span class="fas fa-spinner fa-4x fa-spin"></span><span class="txtcargando">Cargando</span></div>';
        this.init= function(){
        	this.empty().html(sp_html);
        	this.texto = this.find(".txtcargando").first();
        	this.hide();
        }
        this.iniciar = function(texto){
        	this.texto.html(texto);
        	this.show("fade");
        }
        this.fin= function(){
        	this.hide("fade");
        }
        self.init();

        return self;
    };
}( jQuery ));