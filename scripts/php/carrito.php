<?php
	error_reporting(E_ALL);
	session_start();
	/*session_destroy();
	die();*/
	try{
		if(isset($_POST["action"])){
			if(!isset($_SESSION["carrito"])){
//				$newcart = array("products"=>array());
				$_SESSION["carrito"]= json_encode(array());

			}
			switch($_POST["action"]){
				case "get":
					echo json_encode(array("productos"=>json_decode($_SESSION["carrito"])));
				break;
				case "set":
					$_SESSION["carrito"]= json_encode($_POST["data"]);
					echo json_encode(array("set"=>true));
				break;
				case "reset":
					$_SESSION["carrito"]= json_encode(array());
					echo json_encode(array("reset"=>true));
				break;
				default:	echo json_encode(array("error"=>"Instrucción desconocida: '$_POST[action]'."));
				break;
			}
		} else echo json_encode(array("error"=>"No recibió instrucción: 'action'."));
	} catch(Exception $ex){
		echo json_encode(array("error"=>$ex->getMessage()));
	} finally{
		exit(0);
	}
?>