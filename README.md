**Don baratón app**

Es una app de e-commerce para el gran negocio de don Pepe
*Tiendas Don Baratón S.A. de C.V. 2019*

---

## Descripción

Es una aplicación web para el comercio electrónico de los productos de tiendas **Don Baraton**

*Características:*

1. Estilo moderno y minimalísta.
2. Tipo SPA (Single Page App).
3. Responsiva e intuitiva.
4. Basada en jQuery, jQuery UI y Bootstrap.
5. Utiliza tecnología ajax para hacer cargas parciales sin interrumpir el flujo de la aplicación.

---

## Requisitos

Software mínimo para ejecutar esta webapp:

1. **Servidor Apache** para ejecutar los script de **PHP**.
2. **NodeJS** para tener el manejador de paquetes.
3. **Node Package Manager (npm)** para instalar bower.
4. **bower** para poder agregar toda utilería de terceros usada en este proyecto
5. **git**,**sourcetree** o cualquier herramienta que integre la consola de comandos de git.

---

## Instalación
1. **obtener el clone del proyecto**
	*git clone https://jsanjuancismx@bitbucket.org/jsanjuancismx/rappichallenge.git*
2. **Entrar a la carpeta del proyecto** 
3. **Usar *bower install* para instalar todas las dependencias.
4. **Arrancar aplicación** desde el servidor local.


..................................................................................

### LSC. Julio César Sanjuán Hdz ###
..................................................................................
